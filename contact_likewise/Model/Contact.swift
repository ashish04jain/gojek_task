//
//  Contact.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import ObjectMapper

class Contact: NSObject, Mappable {
    
    var id: Int!
    var firstName: String?
    var lastName: String?
    var email: String?
    var mobile: String?
    var profileUrl: URL?
    var favorite: Bool?
    
    var createdAt: Date!
    var updatedAt: Date!
    
    override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        profileUrl <- (map["profile_pic"], URLTransform())
        
        if let fav = map["favorite"].currentValue as? Int {
            favorite = fav.toBool()
        }
        
        email <- map["email"]
        mobile <- map["phone_number"]

        
        if let dateString = map["created_at"].currentValue as? String {
            createdAt = getDateFromTZString(string: dateString)
        }
        
        if let dateString = map["updated_at"].currentValue as? String {
            updatedAt = getDateFromTZString(string: dateString)
        }
    }
    
    func loadParams(firstName: String?, lastName:String?, email:String?, mobile:String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.mobile = mobile
    }
    
    func validateParams() -> AlamofireAPIResponse {
        guard firstName?.count != 0 else {
            return AlamofireAPIResponse(response: nil, errorCode: 0, errorMessage: AppConstants.Error.enterFirstNameErrorMsg, successful: false)
        }
        
        guard lastName?.count != 0 else {
            return AlamofireAPIResponse(response: nil, errorCode: 0, errorMessage: AppConstants.Error.enterLastNameErrorMsg, successful: false)
        }
        
        if email?.count ?? 0 > 0 {
            guard email!.isValidEmail() else {
                return AlamofireAPIResponse(response: nil, errorCode: 0, errorMessage: AppConstants.Error.enterValidEmailErrorMsg, successful: false)
            }
        }
        
        return AlamofireAPIResponse(response: nil, errorCode: 1, errorMessage: "", successful: true)
    }
    
}


