//
//  Constants.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import UIKit

typealias CompletionBlock = (AlamofireAPIResponse) -> Void
typealias JSON = AnyObject
typealias JSONDictionary = Dictionary<String, JSON>
typealias JSONArray = Array<JSON>

struct Controllers {

    struct navigationTitle {
        static let home = "NYT"
        static let search = "Search"
        static let contact = "Contact"
    }
}

// MARK: APIKeys
struct Keys {

    static let nytAPIKey = "VEAZqWLDtdfcssMMRjDTsJKQx1L0qBnN"
    static let nytAPISecret = "gmPvdnDzygcW67Ss"
}

enum PopularType: String {
    case viewed, shared, emailed
    
    var title: String {
        get {
            switch self {
            case .viewed:
                return "Most Viewed"
            case .shared:
                return "Most Shared"
            case .emailed:
                return "Most Emailed"
            }
        }
    }
}

// MARK :General Constants
struct AppConstants {
    
    struct Error {
        static let networkMsg = "Please check your internet connection and try again!"
        static let generalMsg = "We are notified and working on it, we will be back soon! Please try again later"
        static let enterFirstNameErrorMsg = "Please enter your name"
        static let enterValidEmailErrorMsg = "Please enter a vaild email address"
        static let enterLastNameErrorMsg = "Please enter a last name"
    }
    
    static let offWhiteBGOpacity = 0.4
    
    static let cornerRadius = CGFloat(10.0)
}

//MARK:Placeholder Images
struct PlaceholderImages {
    static let profile = "profile"
}

//MARK:Server Response Key
struct ServerResponseKey {
    static let SUCCESS = "status"
    static let ERROR = "error"
}

struct cellIdentifier {
    static let contactTableViewCell = "ContactTableViewCell"
    static let contactDetailCell = "ContactDetailCell"
}

enum Fields: Int {
    case email, firstName, lastName, phone
    
    var title: String {
        get {
            switch self {
            case .phone:
                return "mobile"
            case .firstName:
                return "First Name"
            case .lastName:
                return "Last Name"
            case .email:
                return "email"
            }
        }
    }
    
}
