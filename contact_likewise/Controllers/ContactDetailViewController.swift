//
//  ContactDetailViewController.swift
//  contact_likewise
//
//  Created by Ashish on 23/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import UIKit

class ContactDetailCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var keyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func load(key:String?, value: String?) {
        keyLabel.text = key ?? ""
        valueLabel.text = value ?? ""
    }
    
}

class ContactDetailViewController: UIViewController {
    
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var favIV: UIImageView!
    @IBOutlet weak var callView: UIStackView!
    @IBOutlet weak var favView: UIStackView!
    @IBOutlet weak var emailView: UIStackView!
    @IBOutlet weak var msgView: UIStackView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var keys = ["mobile", "email"]
    var contact: Contact?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavBar()
        nameLabel.text = fetchName(firstName: contact?.firstName, lastName: contact?.lastName)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadInitials()
        setUpGestures()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addCircularBorder(view: userIV, borderWidth: 2.0, withColor: UIColor.white)
    }
    
    func loadNavBar() {
        self.navigationController?.loadAttributes()
        self.navigationController?.addRightBarButtonTitle(title: "Edit", navigationItem: self.navigationItem)
    }
    
    @objc func didTapOnRightNavigationButton() {
        let vc = EditContactViewController.instantiate(fromAppStoryboard: .Contact)
        vc.contact = contact
        let navVC = UINavigationController(rootViewController: vc)
        self.navigationController?.present(navVC, animated: true, completion: nil)
    }
    
    func loadInitials() {
        userIV.image = #imageLiteral(resourceName: "profile")
        if let url = contact?.profileUrl {
            if UIApplication.shared.canOpenURL(url) {
                userIV.kf.setImage(with: url)
            }
        }

        let fav = contact?.favorite ?? false
        if fav {
            favIV.image = #imageLiteral(resourceName: "fav_btn")
        } else {
            favIV.image = #imageLiteral(resourceName: "unfav_icon")
        }
    }
    
    func setUpGestures() {
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(ContactDetailViewController.didTapToMsg))
        msgView.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(ContactDetailViewController.didTapToCall))
        callView.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(ContactDetailViewController.didTapToEmail))
        emailView.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(ContactDetailViewController.didTapToFav))
        favView.addGestureRecognizer(tap4)
    }
    
    @IBAction func didTapOnDelete(_ sender: Any) {
        self.showSlideNotification(notificationText: "TODO - delete an account")
    }
    
    @objc func didTapToMsg() {
        let contactNumber = contact?.mobile ?? ""
        
        DispatchQueue.main.async {
            self.displayMessageInterface(toRecipients: [contactNumber], subject: "Contact - Likewise", messageBody: "Type your msg here")
        }
    }
    
    @objc func didTapToCall() {
        self.showContactPopup(contact: contact)
    }
    
    @objc func didTapToEmail() {
        self.showEmailPopup(contact: contact)
    }
    
    @objc func didTapToFav() {
        if !ReachabilityManager.sharedInstance.isReachable() {
            showNetworkAlert()
            return
        }
        
        let fav = contact?.favorite ?? false
        if fav {
            favIV.image = #imageLiteral(resourceName: "unfav_icon")
            contact?.favorite = false
        } else {
            favIV.image = #imageLiteral(resourceName: "fav_btn")
            contact?.favorite = true
        }
        updateContact()
    }
    
    func updateContact() {
        self.showProgress(message: nil)
        AlamofireAPIWrapper.sharedInstance.updateContact(id: contact?.id ?? 0, requestDict: ["favorite": contact!.favorite as AnyObject]) { (response) in
            self.hideProgress()
        }
    }
    
}

extension ContactDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keys.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.contactDetailCell, for: indexPath) as! ContactDetailCell
        cell.selectionStyle = .none
        
        let key = keys[indexPath.row]
        if indexPath.row == 0 {
            cell.load(key: key, value: contact?.mobile)
        } else {
            cell.load(key: key, value: contact?.email)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension ContactDetailViewController {
    
    func showContactPopup(contact: Contact?) {
        let contactNumber = contact?.mobile ?? ""
        showActionSheetAlert(target: self, message: contactNumber, title: "Contact Number", buttonTitle1: "copy", buttonTitle2: "call", buttonTitle3: "cancel") { (index) in
            if index == 1 {
                UIPasteboard.general.string = contactNumber
                self.showSlideNotification(notificationText: "Contact number copied to clipboard!")
            } else if index == 2 {
                self.makeCall(toPhone: contactNumber)
            }
        }
    }
    
    func showEmailPopup(contact: Contact?) {
        let contactEmail = contact?.email ?? ""
        if contactEmail.count > 0 {
            showActionSheetAlert(target: self, message: contactEmail, title: "Contact Email", buttonTitle1: "copy", buttonTitle2: "write an email", buttonTitle3: "cancel") { (index) in
                if index == 1 {
                    UIPasteboard.general.string = contactEmail
                    self.showSlideNotification(notificationText: "Contact email copied to clipboard!")
                } else if index == 2 {
                    self.sendEmail(toRecipients: [contactEmail], subject: "Contact - Likewise", messageBody: "")
                }
            }
        } else {
            self.showSlideNotification(notificationText: "This contact doesn't have an email associated to it!")
        }
    }
    
}

