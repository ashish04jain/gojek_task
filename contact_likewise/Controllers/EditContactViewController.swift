//
//  EditContactViewController.swift
//  contact_likewise
//
//  Created by Ashish on 22/07/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import UIKit

class EditContactViewController: UIViewController {
    
    @IBOutlet weak var emailView: CustomTextField!
    @IBOutlet weak var phoneView: CustomTextField!
    @IBOutlet weak var lastNameView: CustomTextField!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var firstNameView: CustomTextField!
    
    var fields: [Fields] = [.firstName, .lastName, .phone, .email]
    var customViews: [CustomTextField]!
    
    var profileImage: UIImage?
    var apiWrapper = AlamofireAPIWrapper.sharedInstance
    let imagePicker = UIImagePickerController()
    
    var contact: Contact?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavBar()
        loadInitials()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addCircularBorder(view: profileIV, borderWidth: 2.0, withColor: UIColor.white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        loadValues()
        firstNameView.textField.becomeFirstResponder()
    }
    
    func loadNavBar() {
        self.navigationController?.loadAttributes()
        self.navigationController?.addLeftBarButtonTitle(title: "Cancel", navigationItem: self.navigationItem)
        self.navigationController?.addRightBarButtonTitle(title: "Done", navigationItem: self.navigationItem)
    }
    
    @objc func didTapOnRightNavigationButton() {
        didTapOnDone()
    }
    
    @objc func didTapOnLeftNavigationButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadInitials() {
        configureComponents()
    }
    
    func configureComponents() {
        imagePicker.delegate = self
        
        customViews = [firstNameView, lastNameView, phoneView, emailView]
        for (index, view) in customViews.enumerated() {
            view.configureField(tag: index, title: fields[index].title)
        }
        emailView.textField.keyboardType = .emailAddress
        emailView.textField.textContentType = UITextContentType.emailAddress
        
        phoneView.textField.keyboardType = .phonePad
    }
    
    func loadValues() {
        profileIV.image = #imageLiteral(resourceName: "profile")
        if let url = contact?.profileUrl {
            if UIApplication.shared.canOpenURL(url) {
                profileIV.kf.setImage(with: url)
            }
        }
        
        firstNameView.setText(text: contact?.firstName ?? "")
        lastNameView.setText(text: contact?.lastName ?? "")
        emailView.setText(text: contact?.email ?? "")
        phoneView.setText(text: contact?.mobile ?? "")
    }
    
    @IBAction func didTapOnGallery(_ sender: Any) {
        showGallery()
    }
    
    func didTapOnDone() {
        let firstName = trimWhitespaces(text: firstNameView.getText())
        let lastName = trimWhitespaces(text: lastNameView.getText())
        let email = trimWhitespaces(text: emailView.getText())
        let mobile = trimWhitespaces(text: phoneView.getText())
        
        let updatedContact = Contact()
        updatedContact.loadParams(firstName: firstName, lastName: lastName, email: email, mobile: mobile)
        
        let response = updatedContact.validateParams()
        if response.isSuccessful == false {
            self.showSlideNotification(notificationText: response.errorMsg!, notificationShowTime: 2.0)
            return
        }
        
        if !ReachabilityManager.sharedInstance.isReachable() {
            showNetworkAlert()
            return
        }
        self.showProgress(message: nil)
        
        
        let requestDict = ["first_name": firstName, "last_name": lastName, "email": email, "phone_number": mobile]
        
        if let _ = contact?.id {
            updateContact(requestDict: requestDict as [String : AnyObject])
        } else {
            addContact(requestDict: requestDict as [String : AnyObject])
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateContact(requestDict: [String: AnyObject]) {
        AlamofireAPIWrapper.sharedInstance.updateContact(id: contact?.id ?? 0, requestDict: requestDict) { (response) in
            self.hideProgress()
        }
    }
    
    func addContact(requestDict: [String: AnyObject]) {
        AlamofireAPIWrapper.sharedInstance.addContact(requestDict: requestDict) { (response) in
            self.hideProgress()
        }
    }
    
}

extension EditContactViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showGallery() {
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = .savedPhotosAlbum
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileIV.image = pickedImage
            self.profileImage = pickedImage
            //upload Image
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}





