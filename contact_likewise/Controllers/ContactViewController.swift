//
//  ContactViewController.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit
import ObjectMapper

class ContactViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var contacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitials()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadNavBar()
    }
    
    func loadNavBar() {
        self.navigationController?.loadAttributes(navBarTintColorDefault: #colorLiteral(red: 0.9647058824, green: 0.968627451, blue: 0.9725490196, alpha: 1))
        self.navigationItem.title = Controllers.navigationTitle.contact
        self.navigationController?.addRightBarButtonImage(rightImage: #imageLiteral(resourceName: "add_icon"), navigationItem: self.navigationItem)
    }
    
    @objc func didTapOnRightNavigationButton() {
        let vc = EditContactViewController.instantiate(fromAppStoryboard: .Contact)
        let navVC = UINavigationController(rootViewController: vc)
        self.navigationController?.present(navVC, animated: true, completion: nil)
    }
    
    func loadInitials() {
        let cell = UINib(nibName: cellIdentifier.contactTableViewCell, bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: cellIdentifier.contactTableViewCell)
        
        fetchContacts()
    }
    
    func fetchContacts() {
        if !ReachabilityManager.sharedInstance.isReachable() {
            showNetworkAlert()
            return
        }
        
        self.showProgress(message: nil)
        AlamofireAPIWrapper.sharedInstance.fetchContacts { (response) in
            if let responseArray = response.responseObject as? JSONArray {
                self.contacts = Mapper<Contact>().mapArray(JSONObject: responseArray)!
            }
            self.tableView.reloadData()
            self.hideProgress()
        }
    }
    
    func fetchContact(id: Int) {
        if !ReachabilityManager.sharedInstance.isReachable() {
            showNetworkAlert()
            return
        }
        
        showProgress(message: nil)
        AlamofireAPIWrapper.sharedInstance.fetchContact(id: id) { (response) in
            if let res = response.responseObject as? JSONDictionary {
                if let contact = Mapper<Contact>().map(JSONObject: res) {
                    self.nextVC(contact: contact)
                } else {
                    self.showSlideNotification(notificationText: "Unable to fetch contact detail")
                }
            }
            self.hideProgress()
        }
    }
    
    func nextVC(contact: Contact) {
        let vc = ContactDetailViewController.instantiate(fromAppStoryboard: .Contact)
        vc.contact = contact
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.contactTableViewCell, for: indexPath) as! ContactTableViewCell
        cell.selectionStyle = .none
        
        cell.contact = contacts[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = contacts[indexPath.row]
        fetchContact(id: contact.id)
    }
    
}

