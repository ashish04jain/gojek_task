//
//  LaunchViewController.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    let appSession = AppSession.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        appSession.loadHomeView()
    }

}

