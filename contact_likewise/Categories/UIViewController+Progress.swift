//
//  UIViewController+Progress.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit
import SVProgressHUD
import MessageUI

extension UIViewController: CustomNavigationBarProtocol, SlideNotificationProtocol, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    public func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
        
        if result == .sent {
            
        }
        
    }
    
    var isModal: Bool {
        return self.presentingViewController?.presentedViewController == self
            || (self.navigationController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController)
            || self.tabBarController?.presentingViewController is UITabBarController
    }
    
    //MARK: progress bar functionality
    func showProgress(message: String?) {
        self.setUp()
        if let msg = message {
            SVProgressHUD.show(withStatus: msg)
        } else {
            SVProgressHUD.show()
        }
    }
    
    func showProgressForCompletion(completed: Float) {
        self.setUp()
        SVProgressHUD.showProgress(completed)
    }
    
    func hideProgress() {
        SVProgressHUD.dismiss()
    }
    
    func hideProgressWithSuccess(message: String?) {
        setUp()
        SVProgressHUD.showSuccess(withStatus: message)
    }
    
    func hideProgressWithFailure(message: String?) {
        setUp()
        SVProgressHUD.showError(withStatus: message)
    }
    
    private func setUp() {
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setBackgroundColor(UIColor.clear)
        SVProgressHUD.setForegroundColor(AppPallette.secondaryColor)
        SVProgressHUD.setRingThickness(6.0)
        SVProgressHUD.setBackgroundColor(UIColor.black.withAlphaComponent(0.2))
    }
    
    //MARK: alert functionality
    func showErrorAlertWithMsg(msg:String){
        self.showSlideNotification(notificationText: msg, notificationShowTime: 3.0)
    }
    
    func showErrorAlert(){
        //showAlert(target: self, message: AppConstants.Error.generalMsg, title: "Oops...", buttonTitle: "OK")
        self.showSlideNotification(notificationText: AppConstants.Error.generalMsg, notificationShowTime: 3.0)
    }
    
    func showNetworkAlert(){
        //showAlert(target: self, message: AppConstants.Error.networkMsg, title: "Network Error", buttonTitle: "OK")
        self.showSlideNotification(notificationText: AppConstants.Error.networkMsg, notificationShowTime: 3.0)
    }

    
    // MARK: default protocol optional functions
    
    @objc  func didTapOnLeftNavigationButton(customNavigationBar:CustomNavigationBar){
        
    }
    
    @objc  func didTapOnRightNavigationButton(customNavigationBar:CustomNavigationBar){
        
    }
    
    @objc  func didTapOnSearchButton(customNavigationBar:CustomNavigationBar){
        
    }
    
    @objc  func didTapOnMenuButton(customNavigationBar:CustomNavigationBar){
        
    }
    
    //MARK: SlideNotification
    
    func showSlideNotification(notificationText: String, notificationShowTime: TimeInterval = 3.0) {
        let window = AppSession.sharedInstance.appDelegate.window
        let frame = CGRect(x: window!.frame.minX, y: window!.frame.minY, width: window!.frame.width, height: 0)
        let notificationView = SlideNotificationView.loadView(frame)
        notificationView.delegate = self
        window?.addSubview(notificationView)
        notificationView.layoutIfNeeded()
        notificationView.showNotificationView(title: notificationText, delayViewTime: 0.1, notificationShowTime: notificationShowTime)
    }
    
    @objc func didNotificationDismissed(view: SlideNotificationView) {
        
    }
    
    func isSuccessful(responseObject:[String:AnyObject]?) -> Bool{
//        if let _ = responseObject?[ServerResponseKey.SUCCESS]{
//            if let result = responseObject![ServerResponseKey.SUCCESS] as? String{
//                return result == "OK" ? true : false
//            } else if let result = responseObject![ServerResponseKey.SUCCESS] as? Bool{
//                return result == true ? true : false
//            } else {
//                return false
//            }
//        }
        return true
    }
    
    func makeCall(toPhone: String) {
        let urlString = "tel://\(toPhone)"
        
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            //            let alert = showAlert(message: "", title: "Call \(toPhone)")
            //            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            //            let callAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: {action in
            //                if #available(iOS 10, *) {
            //                   UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //                } else {
            //                    UIApplication.shared.openURL(url)
            //                }
            //            })
            //            alert.addAction(cancelAction)
            //            alert.addAction(callAction)
            //            self.present(alert, animated: true, completion: nil)
        } else {
            if toPhone.count > 0 {
                showAlert(target: self, message: "Call not supported", title: "Info", buttonTitle: "OK")
            } else {
                showAlert(target: self, message: "No phone number found!", title: "Info", buttonTitle: "OK")
            }
        }
    }
    
    
    func validateServerResponse(response:AlamofireAPIResponse, showErrorAlert:Bool) -> (Bool, JSONDictionary?){
        if response.isSuccessful {
            let responseObject = response.responseObject as? JSONDictionary
            if self.isSuccessful(responseObject: responseObject){
                return (true, responseObject)
            } else {
                if let _ = responseObject {
                    print(responseObject ?? "")
                    if let errorRes = responseObject!["error"] as? JSONDictionary {
                        let msg = errorRes["message"] as? String
                        let code = errorRes["code"] as? Int
                        if code == 403 {
                            
                        } else {
                            if showErrorAlert {
                                self.showErrorAlertWithMsg(msg: msg ?? AppConstants.Error.generalMsg)
                            }
                        }
                        self.hideProgress()
                    } else {
                        self.hideProgress()
                        if showErrorAlert {
                            self.showErrorAlertWithMsg(msg: AppConstants.Error.generalMsg)
                        }
                    }
                } else {
                    self.hideProgress()
                    if showErrorAlert {
                        self.showErrorAlert()
                    }
                }
                return (false, responseObject)
            }
        } else {
            self.hideProgress()
            if showErrorAlert {
                self.showErrorAlert()
            }
            return (false, nil)
        }
    }
    
    func sendEmail(toRecipients: [String], subject: String, messageBody: String) {
        let mailController  = MFMailComposeViewController()
        mailController.mailComposeDelegate = self
        mailController.setSubject(subject)
        mailController.setToRecipients(toRecipients)
        
        mailController.setMessageBody(messageBody, isHTML: false)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailController, animated: true, completion: nil)
        } else {
            showAlert(target: self, message: "Sorry cannot send mail", title: "Error", buttonTitle: "Ok")
        }
    }
    
    //MARK: MFMailComposeViewControllerDelegate
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    //MARK:end
    
    func displayMessageInterface(toRecipients: [String], subject: String, messageBody: String) {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        //        var composeMsgBody = messageBody + "\n\n\nDevice : " + UIDevice.current.localizedModel + "\niOS Version: " + UIDevice.current.systemVersion
        //
        //        if let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String {
        //            composeMsgBody = composeMsgBody + "\nApp version: " + appVersion
        //        }
        
        composeVC.recipients = toRecipients
        composeVC.body = messageBody
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
}

extension UIView {
    //MARK: SlideNotification
    
    func showSlideNotification(notificationText: String, notificationShowTime: TimeInterval = 2.0) {
        let window = AppSession.sharedInstance.appDelegate.window
        let frame = CGRect(x: window!.frame.minX, y: window!.frame.minY, width: window!.frame.width, height: 0)
        let notificationView = SlideNotificationView.loadView(frame)
        //notificationView.delegate = self
        window?.addSubview(notificationView)
        notificationView.layoutIfNeeded()
        notificationView.showNotificationView(title: notificationText, delayViewTime: 0.1, notificationShowTime: notificationShowTime)
    }
    
//    @objc func didNotificationDismissed(view: SlideNotificationView) {
//
//    }
    
    
}


