//
//  NSDate+Utility.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    func toDateString() -> String { // 2013-05-29
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, YYYY"
        return dateFormatter.string(from: self)
    }

    func toDateTimeString() -> String { // 2013-05-29
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, YYYY hh:mm a"
        return dateFormatter.string(from: self)
    }
      
}
