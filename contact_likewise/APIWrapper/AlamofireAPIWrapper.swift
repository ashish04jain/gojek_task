
//
//  AlamofireAPIWrapper.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit
import Alamofire

let baseURL = "http://gojek-contacts-app.herokuapp.com/"


class AlamofireAPIWrapper: NSObject {
    
    static let sharedInstance = AlamofireAPIWrapper()

    var headers: [String: String] {
        get {
            return ["Content-Type": "application/json", "Accept": "application/json"]
        }
    }
    
    func fetchContacts(responseBlock: @escaping CompletionBlock) {
        let urlString = "contacts.json"
        Alamofire.request(baseURL + urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {(response) -> Void in
            if let error = response.result.error {
                print(error.localizedDescription)
                let apiResponse = AlamofireAPIResponse.init(response: nil, errorCode: (error as NSError).code, errorMessage: error.localizedDescription, successful: false)
                responseBlock(apiResponse)
            } else if let jsonValue = response.result.value {
                print(jsonValue)
                let apiResponse = AlamofireAPIResponse.init(response: jsonValue as JSON?, errorCode: 0, errorMessage: "", successful: true)
                responseBlock(apiResponse)
            }
        })
    }
    
    func fetchContact(id: Int, responseBlock: @escaping CompletionBlock) {
        let urlString = "contacts/\(id).json"
        Alamofire.request(baseURL + urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {(response) -> Void in
            if let error = response.result.error {
                print(error.localizedDescription)
                let apiResponse = AlamofireAPIResponse.init(response: nil, errorCode: (error as NSError).code, errorMessage: error.localizedDescription, successful: false)
                responseBlock(apiResponse)
            } else if let jsonValue = response.result.value {
                print(jsonValue)
                let apiResponse = AlamofireAPIResponse.init(response: jsonValue as JSON?, errorCode: 0, errorMessage: "", successful: true)
                responseBlock(apiResponse)
            }
        })
    }
    
    func updateContact(id: Int, requestDict: [String: AnyObject], responseBlock: @escaping CompletionBlock) {
        let urlString = "contacts/\(id).json"
        Alamofire.request(baseURL + urlString, method: .put, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {(response) -> Void in
            if let error = response.result.error {
                print(error.localizedDescription)
                let apiResponse = AlamofireAPIResponse.init(response: nil, errorCode: (error as NSError).code, errorMessage: error.localizedDescription, successful: false)
                responseBlock(apiResponse)
            } else if let jsonValue = response.result.value {
                print(jsonValue)
                let apiResponse = AlamofireAPIResponse.init(response: jsonValue as JSON?, errorCode: 0, errorMessage: "", successful: true)
                responseBlock(apiResponse)
            }
        })
    }
    
    func addContact(requestDict: [String: AnyObject], responseBlock: @escaping CompletionBlock) {
        let urlString = "contacts.json"
        Alamofire.request(baseURL + urlString, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {(response) -> Void in
            if let error = response.result.error {
                print(error.localizedDescription)
                let apiResponse = AlamofireAPIResponse.init(response: nil, errorCode: (error as NSError).code, errorMessage: error.localizedDescription, successful: false)
                responseBlock(apiResponse)
            } else if let jsonValue = response.result.value {
                print(jsonValue)
                let apiResponse = AlamofireAPIResponse.init(response: jsonValue as JSON?, errorCode: 0, errorMessage: "", successful: true)
                responseBlock(apiResponse)
            }
        })
    }
    
}
