
//
//  FontUtility.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

public func navigationTitleFont(fontName: String = "SFUIText-Regular",size: CGFloat = 26) -> UIFont{
    return UIFont(name: fontName, size: size)!
}

public func navigationBarButtonTitleFont() -> UIFont{
    return UIFont(name: "SFUIText-Regular", size: 16)!
}

public func navigationRightBarButtonTitleFont() -> UIFont {
    return UIFont(name: "SFUIText-Regular", size: 16)!
}

public func placeHolderFont(fontSize: CGFloat = 16.0) -> UIFont{
    return UIFont(name: "SFUIText-Regular", size: fontSize)!
}

public func defaultFont(size: CGFloat, fontName: String = "SFUIText-Regular") -> UIFont {
    return UIFont(name: fontName, size: size)!
}
