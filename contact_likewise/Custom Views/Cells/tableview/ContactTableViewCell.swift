//
//  ContactTableViewCell.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    var contact: Contact? {
        didSet {
            loadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addCornerRadius(view: userImageView, radius: userImageView.frame.width/2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData() {
        userImageView.image = #imageLiteral(resourceName: "profile")
        if let url = contact?.profileUrl {
            if UIApplication.shared.canOpenURL(url) {
                userImageView.kf.setImage(with: url)
            }
        }
        
        nameLabel.text = fetchName(firstName: contact?.firstName, lastName: contact?.lastName)
        
        let fav = contact?.favorite ?? false
        if fav {
            favBtn.setImage(#imageLiteral(resourceName: "fav_icon"), for: .normal)
        } else {
            favBtn.setImage(UIImage(), for: .normal)
        }
    }

}

