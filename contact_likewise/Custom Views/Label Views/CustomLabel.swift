//
//  CustomLabel.swift
//  Apex Investigation
//
//  Created by Ashish on 01/08/18.
//  Copyright © 2018 bitcot. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    @IBInspectable open var characterSpacing: CGFloat = 2.0 {
        didSet {
            let attributedString = NSMutableAttributedString(string: self.text!)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: self.characterSpacing, range: NSRange(location: 0, length: attributedString.length))
            self.attributedText = attributedString
        }
    }
}
