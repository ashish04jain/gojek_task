//
//  CustomTextField.swift
//  iCampSavvy
//
//  Created by Ashish on 16/06/17.
//  Copyright © 2017 bitcot. All rights reserved.
//

import UIKit

@objc protocol CustomTextFieldDelegate:class {
    @objc optional func customTextFieldShouldReturn(customView: CustomTextField) -> Bool
    @objc optional func customTextFieldDidBeginEditing(customView: CustomTextField)
    @objc optional func customTextFieldShouldBeginEditing(customView: CustomTextField) -> Bool
    @objc optional func customTextField(customView: CustomTextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    @objc optional func customTextFieldShouldClear(customView: CustomTextField) -> Bool
    @objc optional func didTapOnEdit(customView: CustomTextField)
}

class CustomTextField: UIView {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: CustomLabel!
    
    var delegate: CustomTextFieldDelegate? = nil
    var errorText = ""
    
    required  init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        loadView()
    }
    
    func loadView(){
        let _ = Bundle.main.loadNibNamed("CustomTextField", owner: self, options: nil)?.first
        view.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        textField.delegate = self
        addSubview(view)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addBottomBorderToView(view: self.view, color: AppPallette.base1)
    }

    func configureField(tag: Int = 0, title: String, placeholder: String = "", keyboardType: UIKeyboardType = .default, returnType: UIReturnKeyType = .done) {
        self.textField.tag = tag
        self.titleLabel.text = title
        self.textField.placeholder = placeholder

        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        self.textField.keyboardType = keyboardType
        self.textField.returnKeyType = returnType
    }
    
    func enableErrorAppearance(enable: Bool, errorText: String = "") {
        if enable {
            //addBorder(view: self, withColor: AppPallette.errorBorderColor)
            self.view.backgroundColor = AppPallette.error2
            self.errorText = errorText
            textField.text = errorText
            textField.textColor = AppPallette.errorBorderColor
        } else {
            self.view.backgroundColor = .white
            textField.textColor = .black
            if textField.text == self.errorText {
                textField.text = ""
            }
            //addBorder(view: self, withColor: AppPallette.base3)
        }
    }
    
    func setText(text: String) {
        textField.text = text
    }
    
    func getText() -> String {
        return textField.text ?? ""
    }
    
}

extension CustomTextField: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        if let _ = delegate {
            if let val = delegate?.customTextFieldShouldReturn?(customView: self) {
                return val
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let _ = delegate {
            if let val = delegate?.customTextFieldDidBeginEditing?(customView: self) {
                return val
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let _ = delegate {
            if let val = delegate?.customTextFieldShouldBeginEditing?(customView: self) {
                return val
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let _ = delegate {
            if let val = delegate!.customTextField?(customView: self, shouldChangeCharactersIn: range, replacementString: string) {
                return val
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    //MARK:end
}
