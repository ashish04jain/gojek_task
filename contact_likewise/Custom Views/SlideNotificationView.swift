//
//  SlideNotificationView.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

protocol SlideNotificationProtocol: AnyObject {
    func didNotificationDismissed(view: SlideNotificationView)
}

class SlideNotificationView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    
    var delegate: SlideNotificationProtocol? = nil
    
    class func loadView(_ frame:CGRect) -> SlideNotificationView {
        let view = Bundle.main.loadNibNamed("SlideNotificationView", owner: self, options: nil)!.first as! SlideNotificationView
        print(view)
        view.frame = frame
        view.loadInitials()
        return view
    }
    
    func loadInitials() {
        contentView.layoutIfNeeded()
        viewTopConstraint.constant = -(contentView.frame.height + 30)
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnContentView))
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height >= 1792 {
            titleTopConstraint.constant = 60
        }
        self.contentView.isUserInteractionEnabled = true
        self.isUserInteractionEnabled = true
        self.contentView.addGestureRecognizer(tapGuesture)
    }
    
    @objc func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
        case UISwipeGestureRecognizer.Direction.right:
            print("Swiped right")
        case UISwipeGestureRecognizer.Direction.down:
            print("Swiped down")
        case UISwipeGestureRecognizer.Direction.left:
            print("Swiped left")
        case UISwipeGestureRecognizer.Direction.up:
            print("Swiped up")
            self.hideNotificationView(delay: 0.4)
        default:
            break
        }
    }
    
    @objc func didTapOnContentView() {
        self.hideNotificationView(delay: 0.4)
    }
    
    func showNotificationView(title: String, delayViewTime: TimeInterval = 0.1, notificationShowTime: TimeInterval = 6.0) {
        titleLabel.text = title
        titleLabel.setLineSpacing(lineSpacing: 4, lineHeightMultiple: 0, alignment: .center)
        titleLabel.layoutIfNeeded()
        self.layoutIfNeeded()
        self.layoutSubviews()
        UIView.animate(withDuration: 0.3, delay: delayViewTime, options: .curveEaseIn, animations: {
            self.viewTopConstraint.constant = 0
            self.layoutIfNeeded()
        }) { (finished) in
            self.hideNotificationView(delay: notificationShowTime)
        }
    }
    
    func hideNotificationView(delay: TimeInterval = 6.0) {
        UIView.animate(withDuration: 1.0, delay: delay, options: .curveEaseOut, animations: {
            self.viewTopConstraint.constant = -(self.contentView.frame.height + 30)
            self.layoutIfNeeded()
        }) { (finished) in
            if let _ = self.delegate {
                self.delegate?.didNotificationDismissed(view: self)
            }
            self.removeFromSuperview()
        }
    }
    
}

