//
//  CustomNavigationBar.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func setNavigationTitle(title:String, navigationItem:UINavigationItem){
        navigationItem.title = title;
    }
    
    func addLeftBarButtonImage(leftImage:UIImage, navigationItem:UINavigationItem){
        let barButton = UIBarButtonItem(image: leftImage, style: UIBarButtonItem.Style.plain, target: self.topViewController, action: Selector(("didTapOnLeftNavigationButton")))
        navigationItem.leftBarButtonItem = barButton
    }
    
    func addCustomLeftBarViewWithImageURL(leftImage:URL, navigationItem:UINavigationItem){
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.image = UIImage(named:PlaceholderImages.profile)
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self.topViewController, action: #selector(UIViewController.didTapOnLeftNavigationButton)))
        
        //let barButton = UIBarButtonItem(image: leftImage, style: UIBarButtonItemStyle.plain, target: self.topViewController, action: Selector(("didTapOnLeftNavigationButton")))
        let barButton = UIBarButtonItem(customView: imageView)
        navigationItem.leftBarButtonItem = barButton
    }
    
    func addLeftBarButtonTitle(title:String,navigationItem:UINavigationItem){
        let barButton = UIBarButtonItem(title: title, style: .plain, target: self.topViewController, action: Selector(("didTapOnLeftNavigationButton")))
        barButton.setTitleTextAttributes([NSAttributedString.Key.font:navigationRightBarButtonTitleFont()], for: UIControl.State.normal)
        navigationItem.leftBarButtonItem = barButton
    }
    
    func addLeftBarButtonImage(image:UIImage, title:String, navigationItem:UINavigationItem){
        let searchButton = UIBarButtonItem(image: image, style: UIBarButtonItem.Style.plain, target: self.topViewController, action: Selector(("didTapOnLeftNavigationButton")))
        let menuButton = UIBarButtonItem(title: title, style: .plain, target: self.topViewController, action: Selector(("didTapOnLeftNavigationButton")))
        navigationItem.leftBarButtonItems = [searchButton,menuButton]
    }
    
    @objc func addRightBarButtonImage(rightImage:UIImage,navigationItem:UINavigationItem){
        let barButton = UIBarButtonItem(image: rightImage, style: UIBarButtonItem.Style.plain, target: self.topViewController, action: Selector(("didTapOnRightNavigationButton")))
        navigationItem.rightBarButtonItem = barButton
    }
    
    func addRightBarButtonTitle(title:String,navigationItem:UINavigationItem, barTintColor: UIColor = AppPallette.navigationTintColor) {
        let barButton = UIBarButtonItem(title: title, style: .plain, target: self.topViewController, action: Selector(("didTapOnRightNavigationButton")))
        barButton.setTitleTextAttributes([NSAttributedString.Key.font:navigationRightBarButtonTitleFont()], for: UIControl.State.normal)
        barButton.tintColor = barTintColor
        navigationItem.rightBarButtonItem = barButton
    }
    
    func addRightBarButtonItems(buttons:[UIBarButtonItem],navigationItem:UINavigationItem){
        navigationItem.rightBarButtonItems = buttons
    }
    
    func addRightBarButtonImages(rightIcon:UIImage, title:String,navigationItem:UINavigationItem){
        let searchButton = UIBarButtonItem(image: rightIcon, style: UIBarButtonItem.Style.plain, target: self, action: Selector(("didTapOnRightNavigationButton:")))
        let menuButton = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: self, action: Selector(("didTapOnRightNavigationButton:")))
        navigationItem.rightBarButtonItems = [searchButton,menuButton]
    }
    
    func applyBgTransparency(){
        navigationBar.shadowImage = UIImage()
        view.backgroundColor = UIColor.clear
        navigationBar.backgroundImage(for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.backgroundColor = UIColor.clear
    }
    
    func applyBgTransparencyWithExtendedBG(){
        navigationBar.isTranslucent = true
        navigationBar.barStyle = .default;
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
    }
    
    func loadAttributes(font: UIFont = navigationTitleFont(), navBarTintColorDefault: UIColor = AppPallette.navigationBarTintColor, navTintColorDefault: UIColor = AppPallette.navigationTintColor, navigationTitleColorDefault: UIColor? = UIColor.black, isTranslucent: Bool = true){
        isNavigationBarHidden = false
        navigationBar.isTranslucent = isTranslucent
        let navBarTintColor = navBarTintColorDefault
        let navTintColor = navTintColorDefault
        let titleAttributes:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font:font,NSAttributedString.Key.foregroundColor:navigationTitleColorDefault ?? UIColor.red]
        applyNavigationBarAttributes(navigationBarTintColor: navBarTintColor, navigationTintColor: navTintColor, navigationTitleAttributes: titleAttributes)
    }
    
    func applyNavigationBarAttributes(navigationBarTintColor: UIColor, navigationTintColor: UIColor, navigationTitleAttributes: [NSAttributedString.Key:AnyObject]){
        navigationBar.barTintColor = navigationBarTintColor
        navigationBar.tintColor = navigationTintColor
        navigationBar.titleTextAttributes = navigationTitleAttributes
    }
    
    func applyNavigationBarAttributes(navigationBarTintColor: UIColor, navigationTintColor: UIColor){
        navigationBar.barTintColor = navigationBarTintColor
        navigationBar.tintColor = navigationTintColor
    }
    
    func addTitleView(titleView:UIView,navigationItem:UINavigationItem){
        navigationItem.titleView = titleView
        navigationItem.titleView?.backgroundColor = UIColor.clear
    }
    
    func hideBottomHairline() {
        self.navigationBar.setBackgroundImage(UIImage(),
                                              for: .any,
                                              barMetrics: .default)
        self.navigationBar.shadowImage = UIImage()
    }
}

protocol CustomNavigationBarProtocol : AnyObject{
    func didTapOnLeftNavigationButton(customNavigationBar:CustomNavigationBar)
    func didTapOnRightNavigationButton(customNavigationBar:CustomNavigationBar)
}

class CustomNavigationBar: UINavigationBar {
    
    var navigationItem:UINavigationItem?
    var delgate : CustomNavigationBarProtocol? = nil
    
    convenience init() {
        self.init(frame: CGRect.zero, target:nil)
    }
    
    init(frame:CGRect,target:UIViewController?){
        super.init(frame:frame)
        self.frame = frame;
        delgate = target
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(){
        navigationItem = UINavigationItem()
        isTranslucent = false
        loadAttributes()
    }
    
    func loadAttributes(){
        let navBarTintColor = AppPallette.navigationBarTintColor;
        let navTintColor = AppPallette.navigationTintColor;
        isTranslucent = false
        let titleAttributes = [NSAttributedString.Key.font:navigationTitleFont(),NSAttributedString.Key.foregroundColor:AppPallette.navigationTitleColor]
        applyNavigationBarAttributes(navigationBarTintColor: navBarTintColor, navigationTintColor: navTintColor, navigationTitleAttributes: titleAttributes)
    }
    
    func applyNavigationBarAttributes(navigationBarTintColor: UIColor, navigationTintColor: UIColor, navigationTitleAttributes: [NSAttributedString.Key:AnyObject]){
        barTintColor = navigationBarTintColor
        tintColor = navigationTintColor
        titleTextAttributes = navigationTitleAttributes
    }
    
    func applyBgTransparency(){
        UINavigationBar.appearance().setBackgroundImage(
            UIImage(),
            for: .any,
            barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        self.isTranslucent = false 
    }
    
    func addLeftButtonTitle(title:String){
        let barButton = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: self, action: #selector(CustomNavigationBar.didTapOnLeftButton))
        barButton.setTitleTextAttributes([NSAttributedString.Key.font:navigationRightBarButtonTitleFont()], for: UIControl.State.normal)
        navigationItem?.leftBarButtonItem = barButton
        barButton.tintColor = AppPallette.secondaryColor
        items = [navigationItem!]
    }
    
    func addRightButtonTitle(title:String){
        let barButton = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: self, action:#selector(CustomNavigationBar.didTapOnRightButton))
        barButton.setTitleTextAttributes([NSAttributedString.Key.font:navigationRightBarButtonTitleFont()], for: UIControl.State.normal)
        barButton.tintColor = AppPallette.secondaryColor
        navigationItem?.rightBarButtonItem = barButton
        items = [navigationItem!]
    }
    
    func addLeftBarButtonImage(leftImage:UIImage){
        let barButton = UIBarButtonItem(image: leftImage, style: UIBarButtonItem.Style.plain, target: self, action:#selector(CustomNavigationBar.didTapOnLeftButton))
        navigationItem?.leftBarButtonItem = barButton
        items = [navigationItem!]
    }
    
    func addCustomRightBarViewWithImageURL(rightImageURL:URL?){
        let imageView = UIImageView(frame: CGRect(x: 0, y: -5, width: 37, height: 37))
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.clipsToBounds = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomNavigationBar.didTapOnRightButton)))
        
        // iOS 11 changes
        
        let widthConstraint = imageView.widthAnchor.constraint(equalToConstant: 40)
        let heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 40)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        /////////////end
        
        if let url = rightImageURL{
//            imageView.setImageWithUrl(url, placeHolderImage: UIImage(named:PlaceholderImages.profile))
        }else{
            imageView.image = UIImage(named:PlaceholderImages.profile)
        }
        let barButton = UIBarButtonItem(customView: imageView)
        navigationItem?.rightBarButtonItem = barButton
        items = [navigationItem!]
    }
    
    func removeRightBarButtonImage(){
        navigationItem?.rightBarButtonItem = nil
        items = [navigationItem!]
    }
    
    func removeLeftBarButtonImage(){
        navigationItem?.leftBarButtonItem = nil
        items = [navigationItem!]
    }
    
    func addRightBarButtonImage(leftImage:UIImage){
        let barButton = UIBarButtonItem(image: leftImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(CustomNavigationBar.didTapOnRightButton))
        navigationItem?.rightBarButtonItem = barButton
        items = [navigationItem!]
    }
    
    func setNavigationTitle(title:String){
        navigationItem!.title = title;
        items = [navigationItem!]
    }
    
    func addTitleView(titleView:UIView){
        navigationItem!.titleView = titleView
        items = [navigationItem!]
    }
    
    func addRightBarButtonImages(menuImage: UIImage, searchImage: UIImage){
        let searchButton = UIBarButtonItem(image: menuImage, style: UIBarButtonItem.Style.plain, target: self, action: Selector(("didTapOnMenu:")))
        
        let menuButton = UIBarButtonItem(image: searchImage, style: UIBarButtonItem.Style.plain, target: self, action: Selector(("didTapOnSearch:")))
        
        navigationItem?.rightBarButtonItems = [searchButton,menuButton]
        items = [navigationItem!]
    }
    
    
    func hideBottomHairline() {
        UINavigationBar.appearance().setBackgroundImage(
            UIImage(),
            for: .any,
            barMetrics: .default)
        
        UINavigationBar.appearance().shadowImage = UIImage()
        //         let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        //        navigationBarImageView!.hidden = true
    }
    
    private func hairlineImageViewInNavigationBar(view: UIView) -> UIImageView? {
        if view.isKind(of: UIImageView.self) && view.bounds.height <= 1.0 {
            return (view as! UIImageView)
        }
        
        let subviews = (view.subviews as [UIView])
        for subview: UIView in subviews {
            if let imageView: UIImageView = hairlineImageViewInNavigationBar(view: subview) {
                return imageView
            }
        }
        
        return nil
    }
    
    //MARK: Action methods
    @objc func didTapOnLeftButton(){
        if delgate != nil{
            delgate?.didTapOnLeftNavigationButton(customNavigationBar: self)
        }
    }
    
    @objc func didTapOnRightButton(){
        if delgate != nil{
            delgate?.didTapOnRightNavigationButton(customNavigationBar: self)
        }
    }
}


