//
//  AppSession.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import UIKit

class AppSession: NSObject {
    
    static let sharedInstance = AppSession()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var contactNavVC: UINavigationController?
    var contactVC: ContactViewController?
    
    override init() {
        super.init()
    }

    func loadHomeView() {
        if contactNavVC == nil {
            contactVC = ContactViewController.instantiate(fromAppStoryboard: .Main)
            contactNavVC = UINavigationController(rootViewController: contactVC!)
        }
        
        let window = appDelegate.window!
        window.rootViewController = contactNavVC
    }
    
}
