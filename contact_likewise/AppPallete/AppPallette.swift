//
//  AppPallette.swift
//  contact_likewise
//
//  Created by Ashish on 22/05/19.
//  Copyright © 2019 ashish. All rights reserved.
//

import Foundation
import UIKit

struct AppPallette {

    //primary color
    static let primaryColor =  #colorLiteral(red: 0.1725490196, green: 0.662745098, blue: 0.8823529412, alpha: 1)
    static let secondaryColor = #colorLiteral(red: 0.2156862745, green: 0.337254902, blue: 0.6117647059, alpha: 1)
    static let tertiaryColor = #colorLiteral(red: 0.4588235294, green: 0.1725490196, blue: 0.6156862745, alpha: 1)
    
    
    // TextField Color
    static let defaulPalceHolderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let defaultTextFieldColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let defaultBorderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    static let error = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
    
    //Hex: fb958B rgb(251, 149, 139)
    static let blueColor = #colorLiteral(red: 0.3137254902, green: 0.5647058824, blue: 0.7725490196, alpha: 1)
    static let whiteColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let blackColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let clearColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    //Base colors
    //#efeeed rgb(239, 238, 237)
    static let base1 = #colorLiteral(red: 0.937254902, green: 0.9333333333, blue: 0.9294117647, alpha: 1)
    //dddcda rgb(221, 220, 218)
    static let base2 = #colorLiteral(red: 0.8666666667, green: 0.862745098, blue: 0.8549019608, alpha: 1)
    //bdbab4 rgb(189, 186, 180)
    static let base3 = #colorLiteral(red: 0.7411764706, green: 0.7294117647, blue: 0.7058823529, alpha: 1)
    //575552 rgb(87, 85, 82)
    static let base4 = #colorLiteral(red: 0.3411764706, green: 0.3333333333, blue: 0.3215686275, alpha: 1)
    
    static let eventColor = #colorLiteral(red: 0.4980392157, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
    
    static let unvalidateColor = #colorLiteral(red: 0.9294117647, green: 0.1098039216, blue: 0.1411764706, alpha: 1)
    
    static let unvalidateButtonColor = #colorLiteral(red: 0.5882352941, green: 0.1098039216, blue: 0.1098039216, alpha: 1)

    //    error colors
    
    //    96c1c1 rgb(150, 193, 193)
    static let error1 = #colorLiteral(red: 0.5882352941, green: 0.7568627451, blue: 0.7568627451, alpha: 1)
    //      fff4f4 rgb(255, 244, 244)
    static let error2 = #colorLiteral(red: 1, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
    //    #961C1C rgb(150, 28, 28)
    static let errorBorderColor = #colorLiteral(red: 0.5882352941, green: 0.1098039216, blue: 0.1098039216, alpha: 1)
    
    // a3a3a3
    static let placeholderColor = #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1)
    
    static let borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    
    static let searchBorderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    
    // 717171
    static let appGrayColor = #colorLiteral(red: 0.4431372549, green: 0.4431372549, blue: 0.4431372549, alpha: 1)
    
    //white translucent
    static let whiteTranslucentColor =  UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: CGFloat(AppConstants.offWhiteBGOpacity))
    
    //navigation bar tint colors
    static let navigationBarTintColor = UIColor.white
    static let navigationTintColor = #colorLiteral(red: 0.3137254902, green: 0.8901960784, blue: 0.7607843137, alpha: 1)
    static let navigationTitleColor = primaryColor
    
    static let tabBarBGColor = secondaryColor
    static let tabSelectedColor = tertiaryColor
    static let tabUnselectedColor = UIColor.white
   
    static let galleryBackgroundColor =  UIColor(red: 65.0/255.0, green: 70.0/255.0, blue: 77.0/255.0, alpha: 1.0)

    
    //Experience Button
    static let expUnSelectedColor = UIColor.white
    
    //CustomTextFieldBorderColor
    static let customTextFieldBorderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.6646243579)
    
    //RadioButton
    static let radioButtonUncheckColor = UIColor.white
}
